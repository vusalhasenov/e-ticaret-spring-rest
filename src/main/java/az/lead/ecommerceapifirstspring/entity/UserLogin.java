package az.lead.ecommerceapifirstspring.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user_login")
public class UserLogin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "status", insertable = false)
    private Integer status;

    @OneToOne(mappedBy = "userLoginId", fetch = FetchType.LAZY)
    private User user;
}
