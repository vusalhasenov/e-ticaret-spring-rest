package az.lead.ecommerceapifirstspring.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class CommonAuditEntity {
    @Column(name = "insert_date",updatable = false)
    private LocalDateTime insertDate;

    @Column(name = "update_date",insertable = false)
    private LocalDateTime updateDate;

    @CreatedBy
    @Column(name = "insert_by",updatable = false)
    private String insertBy;

    @LastModifiedBy
    @Column(name = "update_by",insertable = false)
    private String  updateBy;

    @PrePersist
    public void persist(){
        insertDate=LocalDateTime.now();
    }

    @PreUpdate
    public void update(){
        updateDate=LocalDateTime.now();
    }
}
