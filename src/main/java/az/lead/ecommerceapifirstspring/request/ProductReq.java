package az.lead.ecommerceapifirstspring.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ProductReq {

    @NotNull(message = "product type can not be null")
    @Positive(message = "product type can not be negative")
    private Long productTypeId;
    @NotNull(message = "Unit can not be null")
    @Positive(message = "Unit can not be negative")
    private Long unitId;
    @NotBlank(message = "name can't be null")
    private String name;
    @NotNull(message = "Price can not be null")
    @Positive(message = "Price can not be negative")
    private Double price;
    @NotBlank(message = "code cant be null")
    private String code;


}
