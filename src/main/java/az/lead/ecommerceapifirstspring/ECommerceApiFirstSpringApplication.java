package az.lead.ecommerceapifirstspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ECommerceApiFirstSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(ECommerceApiFirstSpringApplication.class, args);
    }

}
