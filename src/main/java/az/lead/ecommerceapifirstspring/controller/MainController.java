package az.lead.ecommerceapifirstspring.controller;

import az.lead.ecommerceapifirstspring.enums.ResultCode;
import az.lead.ecommerceapifirstspring.model.ApiResult;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping(value = {"/","/csrf"})
    public ApiResult ok(){
        return new ApiResult(ResultCode.OK);
    }

    @GetMapping("/current-user")
    public String username(){
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
