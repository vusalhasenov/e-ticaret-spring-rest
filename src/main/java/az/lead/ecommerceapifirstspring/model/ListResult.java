package az.lead.ecommerceapifirstspring.model;

import az.lead.ecommerceapifirstspring.enums.ResultCode;
import lombok.Getter;

import java.util.List;

@Getter
public class ListResult<M> extends ApiResult{
    private final List<M> list;

    public ListResult(ResultCode resultCode, List<M> list) {
        super(resultCode);
        this.list = list;
    }
    public ListResult(List<M> data){
        super(ResultCode.OK);
        this.list=data;
    }
}
