package az.lead.ecommerceapifirstspring.repository;

import az.lead.ecommerceapifirstspring.entity.UserLogin;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserLoginRepo extends CrudRepository<UserLogin,Long> {

    Optional<UserLogin> findByUsername(String username);
}
