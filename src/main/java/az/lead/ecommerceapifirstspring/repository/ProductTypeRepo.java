package az.lead.ecommerceapifirstspring.repository;

import az.lead.ecommerceapifirstspring.entity.ProductType;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProductTypeRepo extends CrudRepository<ProductType,Long> {
    @Override
    Optional<ProductType> findById(Long id);


}
