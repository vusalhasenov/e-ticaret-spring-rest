package az.lead.ecommerceapifirstspring.repository;

import az.lead.ecommerceapifirstspring.entity.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepo extends CrudRepository<Product,Long> {

    List<Product> findAll();

    @Override
    Optional<Product> findById(Long aLong);


    @Query(value = "select P from Product P where P.productCode=?1 and P.isActive=1")
    Optional<Product> findByProductCode(String productCode);

    boolean existsByProductCodeAndIdNot(String productCode,Long id);




}
