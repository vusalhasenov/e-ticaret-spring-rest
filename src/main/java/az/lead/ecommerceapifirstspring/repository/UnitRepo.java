package az.lead.ecommerceapifirstspring.repository;

import az.lead.ecommerceapifirstspring.entity.Unit;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UnitRepo extends CrudRepository<Unit,Long> {
    @Override
    Optional<Unit> findById(Long id);
}
