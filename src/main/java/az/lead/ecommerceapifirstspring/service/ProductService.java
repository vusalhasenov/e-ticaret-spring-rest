package az.lead.ecommerceapifirstspring.service;

import az.lead.ecommerceapifirstspring.dto.ProductDto;
import az.lead.ecommerceapifirstspring.request.ProductReq;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<ProductDto> findAll();
    ProductDto findById(Long id);

    ProductDto save(ProductReq productReq);

    ProductDto update(Long id,ProductReq productReq);

}
