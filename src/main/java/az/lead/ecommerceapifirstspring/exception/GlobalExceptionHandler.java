package az.lead.ecommerceapifirstspring.exception;

import az.lead.ecommerceapifirstspring.enums.ResultCode;
import az.lead.ecommerceapifirstspring.model.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.naming.AuthenticationException;


@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler  {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(BaseException.class)
    public ApiResult dataNotFoundException(BaseException exception) {
        log.warn("data not found exception : code {}, message {} ",
                exception.getResult().getCode(),
                exception.getResult().getValue());
        return new ApiResult(exception.getResult());
    }
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(InsufficientAuthenticationException.class)
    public ApiResult unauthorized(InsufficientAuthenticationException exception) {
        exception.printStackTrace();
        return new ApiResult(ResultCode.INVALID_CREDENTIALS);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResult invalidArgument(MethodArgumentNotValidException exception){
        var message=exception.getBindingResult().getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .findFirst();

        return message.map(s -> new ApiResult(ResultCode.INVALID_FIELD_VALUE.getCode(), s))
                .orElseGet(() -> new ApiResult(ResultCode.INVALID_FIELD_VALUE));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ApiResult urlNotFound(NoHandlerFoundException exception){
        return new ApiResult(ResultCode.URL_NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ApiResult methodNotSupported(HttpRequestMethodNotSupportedException exception){

        return new ApiResult(ResultCode.REQUEST_METHOD_NOT_SUPPORTED);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ApiResult unHandledExceptions(Exception exception) {
        exception.printStackTrace();
        return new ApiResult(ResultCode.SYSTEM_ERROR);
    }

}
