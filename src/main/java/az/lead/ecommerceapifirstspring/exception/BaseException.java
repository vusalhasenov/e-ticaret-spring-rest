package az.lead.ecommerceapifirstspring.exception;

import az.lead.ecommerceapifirstspring.enums.ResultCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BaseException extends RuntimeException{
    private final ResultCode result;

}
