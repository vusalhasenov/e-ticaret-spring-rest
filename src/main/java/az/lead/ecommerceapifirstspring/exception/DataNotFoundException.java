package az.lead.ecommerceapifirstspring.exception;

import az.lead.ecommerceapifirstspring.enums.ResultCode;
import lombok.Data;

public class DataNotFoundException extends BaseException{


    public DataNotFoundException(ResultCode resultCode) {
        super(resultCode);
    }

    public static DataNotFoundException productNotFound(){
        return new DataNotFoundException(ResultCode.PRODUCT_NOT_FOUND);
    }

    public static DataNotFoundException productTypeNotFound(){
        return new DataNotFoundException(ResultCode.PRODUCT_TYPE_NOT_FOUND);
    }
    public static DataNotFoundException unitNotFound(){
        return new DataNotFoundException(ResultCode.UNIT_NOT_FOUND);
    }


    public static DataNotFoundException userNotFound() {
        return new DataNotFoundException(ResultCode.USER_NOT_FOUND);
    }
}
